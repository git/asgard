module apiote.xyz/p/asgard

go 1.18

require (
	apiote.xyz/p/go-dirty v0.0.0-20211218161334-e486e7b5cf43
	apiote.xyz/p/gott/v2 v2.0.3
	filippo.io/age v1.1.1
	git.sr.ht/~sircmpwn/getopt v1.0.0
	github.com/ProtonMail/gopenpgp/v2 v2.7.3
	github.com/bytesparadise/libasciidoc v0.8.0
	github.com/emersion/go-imap v1.2.1
	github.com/emersion/go-imap-move v0.0.0-20210907172020-fe4558f9c872
	github.com/emersion/go-message v0.17.0
	github.com/emersion/go-msgauth v0.6.6
	github.com/emersion/go-sasl v0.0.0-20220912192320-0145f2c60ead
	github.com/emersion/go-smtp v0.18.0
	github.com/mattn/go-sqlite3 v1.14.17
	notabug.org/apiote/gott v1.1.2
)

require (
	github.com/ProtonMail/go-crypto v0.0.0-20230828082145-3c4c8a2d2371 // indirect
	github.com/ProtonMail/go-mime v0.0.0-20230322103455-7d82a3887f2f // indirect
	github.com/alecthomas/chroma v0.10.0 // indirect
	github.com/alecthomas/chroma/v2 v2.8.0 // indirect
	github.com/cloudflare/circl v1.3.3 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dlclark/regexp2 v1.10.0 // indirect
	github.com/emersion/go-textwrapper v0.0.0-20200911093747-65d896831594 // indirect
	github.com/go-task/slim-sprig v0.0.0-20230315185526-52ccab3ef572 // indirect
	github.com/google/pprof v0.0.0-20230821062121-407c9e7a662f // indirect
	github.com/mna/pigeon v1.1.0 // indirect
	github.com/modocache/gover v0.0.0-20171022184752-b58185e213c5 // indirect
	github.com/onsi/ginkgo/v2 v2.12.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	github.com/sozorogami/gover v0.0.0-20171022184752-b58185e213c5 // indirect
	golang.org/x/crypto v0.12.0 // indirect
	golang.org/x/mod v0.12.0 // indirect
	golang.org/x/sys v0.11.0 // indirect
	golang.org/x/text v0.12.0 // indirect
	golang.org/x/tools v0.12.0 // indirect
	golang.org/x/xerrors v0.0.0-20220609144429-65e65417b02f // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
