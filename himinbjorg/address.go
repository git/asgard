package himinbjorg

import (
	"fmt"

	"github.com/emersion/go-imap"
)

func MakeNameAddress(a *imap.Address, encode bool) string {
	personalName := ""
	if encode {
		fields := a.Format()
		personalName = fields[0].(string)
	} else {
		personalName = a.PersonalName
	}
	if personalName != "" {
		return fmt.Sprintf("%s <%s>", personalName, a.Address())
	} else {
		return a.Address()
	}
}
