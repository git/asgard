package himinbjorg

import (
	"time"
)

type Message struct {
	ID       string
	Subject  string
	Body     string
	Date     time.Time
	Dkim     bool
	Sender   string
	Category string
	Thread   string
}

func (m Message) FormatDate() string {
	return m.Date.Format(time.RFC822Z)
}

func (m Message) RESubject() string {
	if m.Subject[:3] != "Re:" {
		return "Re: " + m.Subject
	} else {
		return m.Subject
	}
}
