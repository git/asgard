package himinbjorg

type KnownAddress struct {
	AddressFrom string
	AddressTo   string
	Ban         bool
}

func (a KnownAddress) empty() bool {
	return a.AddressFrom == "" && a.AddressTo == ""
}
