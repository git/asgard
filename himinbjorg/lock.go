package himinbjorg

import (
	"math/rand"
	"strconv"
	"time"
)

type Lock struct {
	Address   string
	Token     string
	Date      time.Time
	Recipient string
}

func NewLock(address, recipient string) Lock {
	token := strconv.FormatUint(rand.Uint64(), 16)
	lock := Lock{
		Address:   address,
		Token:     token,
		Date:      time.Now(),
		Recipient: recipient,
	}
	return lock
}

func (l Lock) Empty() bool {
	return l.Address == "" && l.Token == "" && l.Date.IsZero()
}
