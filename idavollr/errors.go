package idavollr

import (
	"fmt"
)

type EmptyBoxError struct{}

func (EmptyBoxError) Error() string {
	return ""
}

type MalformedMessageError struct {
	MessageID string
	Cause     error
}

func (e MalformedMessageError) Error() string {
	return fmt.Sprintf("Malformed message %s: %s", e.MessageID, e.Cause.Error())
}
