= asgard
apiote <me@apiote.xyz>
:toc:

asgard is a suite of modules based on email workflow.

hermodr (Hermóðr, the messenger) forwards messages from IMAP inbox to another address; it’s designed to be used with https://github.com/emersion/hydroxide[hydroxide].

mimir (Mímir, renowned for his knowledge) is a client-side topic-based mailing list and archive; it acts as a mailing list, forwarding messages based on their In-Reply-To header, and stores them in public archive. Best used as a companion to an open source project.

tyr (Týr, valorous and powerful) is an address-based filter; it lets messages pass through if the pair (sender, recipient) is allowed, directs messages to junk if the pair is explicitly not allowed, and sends a notification to sender if the pair is not known.

vor (Vör, associated with wisdom) is a client-side address-based mailing list. It forwards messages sent to a specific address to all interested parties.

eostre (Ēostre, spring goddess) donwloads journal entries in asciidoc and converts them to html.

== Building

Because asgard uses generics, go >= 1.18 is needed (currently in beta).

All You have to do is run `go build`.

== Running

----
asgard serve
	serves the public archive of mimir, and self-release webpage for tyr

asgard hermodr
	runs one-time forward of hermodr

asgard mimir
	runs one-time archiving and forwarding of mimir

asgard tyr
	runs on time filter of tyr
asgard tyr list
	lists currently quarantined messages
asgard release ID [recipient]
	releases message from quarantine (and saves rule); if recipient is not given, sender can send messages to any address directed to this inbox
asgard offend ID
	moves message to junk and saves sender as known offender
----

== Configuration

asgard is configured with a configuration file (currently read from current directory). Example file can be found in `config_example.dirty`. All fields are mandatory.

== Contribute

This project uses The Code of Merit, which is available as CODE_OF_CONDUCT file.

The roadmap is available in `CHANGELOG.adoc` file and—although it’s not set in stone—feature requests are highly discouraged. Contributions, however, are welcome as patches; please send them to `asgard@git.apiote.xyz` using `git send-email`. Patches must include a sign-off to certify agreement to https://developercertificate.org/[Developer Certificate of Origin].

All communication—questions, bugs, etc.—should go through the mailing list available at `asgard@git.apiote.xyz`. Note that all communication will be made public at https://asgard.apiote.xyz/.

== Mirrors

The canonical repository for this project is https://git.apiote.xyz/asgard.git it’s mirrored at https://notabug.org/apiote/asgard

Mirrors exist solely for the sake of the code and any additional functions provided by third-party services (including but not limited to issues and pull requests) will not be used and will be ignored.

== Licence

----
asgard Copyright (C) 2021–2022 apiote

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
----
